A Roman governor was an official either elected or appointed to be the chief administrator of Roman law throughout one or more of the many provinces constituting the Roman Empire. A Roman governor is also known as a propraetor or proconsul.

The generic term in Roman legal language was Rector provinciae, regardless of the specific titles, which also reflect the province's intrinsic and strategic status, and corresponding differences in authority.

By the time of the early empire, there were two types of provinces—senatorial and imperial—and several types of governor would emerge. Only proconsuls and propraetors fell under the classification of promagistrate. 